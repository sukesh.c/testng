package com.testcase;

import com.api.CodeProperties;
import com.api.CodePropertiesLookup;
import org.testng.Assert;
import org.testng.annotations.Test;
/*When the depends on method not availabale then use  hte  annotation  ignoreMissingDependencies =true ;
this will ignore the  missing depeneefncy*/
public class TestDependsOnMethods {
    private String currencyCode = "" ;
    @Test
    public void testCaseB(){
        CodeProperties testCode= CodePropertiesLookup.getCodeProperties("XDR");
        Assert.assertEquals(testCode.getCurrencyCode(),"960");
        Assert.assertEquals(testCode.getSymbol(),"XDR");
        Assert.assertEquals(testCode.getFractionDigits(),-1);
        Assert.assertEquals(testCode.getCurrencyName(),"SDR (Special Drawing Right)");
        currencyCode="414";

    }
    @Test(dependsOnMethods={"testCaseB"})
    public void testCaseA(){
        CodeProperties testCode= CodePropertiesLookup.getCodeProperties(currencyCode);
        Assert.assertEquals(testCode.getCurrencyCode(),"414");
        Assert.assertEquals(testCode.getSymbol(),"KWD");
        Assert.assertEquals(testCode.getFractionDigits(),3);
        Assert.assertEquals(testCode.getCurrencyName(),"Kuwaiti Dinar");

    }
    @Test(dependsOnMethods={"testCaseA","testCaseB"})
    public void testCaseC(){
        CodeProperties testCode= CodePropertiesLookup.getCodeProperties(currencyCode);
        Assert.assertEquals(testCode.getCurrencyCode(),"414");
        Assert.assertEquals(testCode.getSymbol(),"KWD");
        Assert.assertEquals(testCode.getFractionDigits(),3);
        Assert.assertEquals(testCode.getCurrencyName(),"Kuwaiti Dinar");

    }
}
