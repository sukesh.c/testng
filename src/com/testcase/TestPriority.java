package com.testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestPriority extends Assert {
    // System.setProperty("webdriver.gecko.driver", "D:\\Sukesh\\Study\\testng\\lib\\geckodriver.exe");
    private WebDriver driver=null;

@Test (priority=1)
    public void testCaseWebDriver(){
    // change the location to your project/lib/
    System.setProperty("webdriver.gecko.driver" , "D:\\\\Sukesh\\Study\\testng\\lib\\geckodriver.exe");

    driver=new FirefoxDriver();

}
@Test(priority=2)
    public void testNavigate(){
    driver.get("https://www.yahoo.com/");
}
@Test (priority=3)
    public void testLogin(){
    driver.findElement(By.id("uh-signin")).click();
    driver.quit();
}
}
