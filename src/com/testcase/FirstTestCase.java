package com.testcase;

import com.api.CodeProperties;
import com.api.CodePropertiesLookup;
import org.testng.annotations.Test;

public class FirstTestCase {
/*
*
* Test NG : @test  annotation method signatures are always  public void or public static void
* TestNG Q:
* */
@Test
public void getData(){
    CodeProperties obj =  CodePropertiesLookup.getCodeProperties("AOA");
    System.out.println("Currency Code :"+obj.getCurrencyCode());
    System.out.println("Currency Code :"+obj.getCurrencyName());
    System.out.println("Currency Code :"+obj.getFractionDigits());
    System.out.println("Currency Code :"+obj.getSymbol());
}@Test
    public void getDataTwo(){
        CodeProperties obj =  CodePropertiesLookup.getCodeProperties("XOF");
        System.out.println("Currency Code :"+obj.getCurrencyCode());
        System.out.println("Currency Code :"+obj.getCurrencyName());
        System.out.println("Currency Code :"+obj.getFractionDigits());
        System.out.println("Currency Code :"+obj.getSymbol());
    }
    @Test
    public void getDataThree(){
        CodeProperties obj =  CodePropertiesLookup.getCodeProperties("ARS");
        System.out.println("Currency Code :"+obj.getCurrencyCode());
        System.out.println("Currency Code :"+obj.getCurrencyName());
        System.out.println("Currency Code :"+obj.getFractionDigits());
        System.out.println("Currency Code :"+obj.getSymbol());
    }
}
