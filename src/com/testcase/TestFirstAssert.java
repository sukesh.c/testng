package com.testcase;

import com.api.CodeProperties;
import com.api.CodePropertiesLookup;
import org.testng.Assert;
import org.testng.annotations.Test;
/*
* Assert Class  is most important class
*
*protected Assert() -- Constructor
*  All Methods in  Assert class  are  static void
*
*
* */

public class TestFirstAssert {
    @Test
public void testCurrencyCode(){
     CodeProperties testCode= CodePropertiesLookup.getCodeProperties("XDR");
      Assert.assertEquals(testCode.getCurrencyCode(),"960");
      Assert.assertEquals(testCode.getSymbol(),"XDR");
      Assert.assertEquals(testCode.getFractionDigits(),-1);
      Assert.assertEquals(testCode.getCurrencyName(),"SDR (Special Drawing Right)");

}
    @Test
    public void testNumCode(){
        CodeProperties testCode= CodePropertiesLookup.getCodeProperties("414");
        Assert.assertEquals(testCode.getCurrencyCode(),"414");
        Assert.assertEquals(testCode.getSymbol(),"KWD");
        Assert.assertEquals(testCode.getFractionDigits(),3);
        Assert.assertEquals(testCode.getCurrencyName(),"Kuwaiti Dinar");

    }

}
