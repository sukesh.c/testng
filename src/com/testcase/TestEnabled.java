package com.testcase;

import com.api.CodeProperties;
import com.api.CodePropertiesLookup;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestEnabled {

    private String currencyCode = "" ;
    @Test(invocationCount=2)
    public void testCaseE(){
        CodeProperties testCode= CodePropertiesLookup.getCodeProperties("XDR");
        Assert.assertEquals(testCode.getCurrencyCode(),"960");
        Assert.assertEquals(testCode.getSymbol(),"XDR");
        Assert.assertEquals(testCode.getFractionDigits(),-1);
        Assert.assertEquals(testCode.getCurrencyName(),"SDR (Special Drawing Right)");
        currencyCode="414";


    }
    @Test(dependsOnMethods={"testCaseE"})
    public void testCaseF(){
        CodeProperties testCode= CodePropertiesLookup.getCodeProperties(currencyCode);
        Assert.assertEquals(testCode.getCurrencyCode(),"414");
        Assert.assertEquals(testCode.getSymbol(),"KWD");
        Assert.assertEquals(testCode.getFractionDigits(),3);
        Assert.assertEquals(testCode.getCurrencyName(),"Kuwaiti Dinar");

    }
    @Test(dependsOnMethods={"testCaseE","testCaseF"},invocationCount = 10,alwaysRun = true,enabled = false)
    public void testCaseG(){
        CodeProperties testCode= CodePropertiesLookup.getCodeProperties(currencyCode);
        Assert.assertEquals(testCode.getCurrencyCode(),"414");
        Assert.assertEquals(testCode.getSymbol(),"KWD");
        Assert.assertEquals(testCode.getFractionDigits(),3);
        Assert.assertEquals(testCode.getCurrencyName(),"Kuwaiti Dinar");
        System.out.println(currencyCode);

    }
}
